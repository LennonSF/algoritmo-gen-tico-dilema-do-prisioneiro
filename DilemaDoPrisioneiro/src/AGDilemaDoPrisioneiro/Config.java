/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

/**
 *
 * @author LennonSF
 */
public class Config {
    public static double mtProbability = 0.01; // probabilidade de mutação
    public static int PopulationSize = 40;
    public static int PS10 = PopulationSize/10; // PS - 10 porcento do tamanho da população 
    public static int PS80 = PopulationSize/10 * 8; // 80 porcento do tamanho da população
    public static int CromossomeSize = 30;
    public static int ChildPopSize = 20; // tamanho da população de proles
    public static int MatingPoolSize = 20; // tamanho da população de pais que vão cruzar
    public static int RingueSize = 3; // tamanho do ringue onde os pais que vão cruzar competem
}

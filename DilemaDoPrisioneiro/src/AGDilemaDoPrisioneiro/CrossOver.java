/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author lennonsalesfurtado
 */
public class CrossOver {

    private HashMap<Integer, Integer> Individual1;
    private HashMap<Integer, Integer> Individual2;
    private int[][] offspring;
    private int[] child1 , child2;
    public static int childCounter;
//    MersenneTwisterFast r;

    public CrossOver() {
        Individual1 = new HashMap<Integer, Integer>();
        Individual2 = new HashMap<Integer, Integer>();
        offspring = new int[Config.ChildPopSize][Config.CromossomeSize];
        child1 = new int[Config.CromossomeSize];
        child2 = new int[Config.CromossomeSize];
        childCounter = 0;
    }

// Seleciona os Genes de dois individuos que vão participar do cruzamento
    public void SelectGenes(int[] ind1, int[] ind2) {
        int cind1 = 0; //contador dos genes selecionados do individuo.
        Individual1.clear();
        Individual2.clear(); // Limpa os hashs

        while (cind1 < Config.ChildPopSize) {
            for (int i = 0; i < ind1.length; i++) {
                if ((Main.r.nextDouble() <= 0.5) && (!Individual1.containsKey(i))) { // probabilidade do gene ser selecionado
                    cind1++;
                    Individual1.put(i, ind1[i]);
                    Individual2.put(i, ind2[i]);
                }
                if (cind1 == Config.ChildPopSize) {
                    break;
                }
            }
        }

        MedianCrossOver(ind1,ind2,Individual1, Individual2);
    }

// Tira a mediana do cruzamento e preeenche o filho e as proles
    public void MedianCrossOver(int[] father1 , int[] father2, HashMap id1, HashMap id2) {
        child1 = father1.clone();  // Copia todos os genes do pai para o filho e depois apenas altera os genes modificados pelo cruzamento por mediana
        child2 = father2.clone();  // Copia todos os genes do pai para o filho e depois apenas altera os genes modificados pelo cruzamento por mediana
        //A função clone passa os valores do vetor pai para o filho. E não a referência pois a referência seria alterada caso fosse passada quando o filho fosse alterado. Assim pai e filho seriam identicos 
        Iterator itr1 = id1.keySet().iterator();
        Iterator itr2 = id2.keySet().iterator();
        
        if (Main.r.nextDouble() <= 0.8) {  
        while (itr1.hasNext()) {
            int key1 = Integer.parseInt(itr1.next().toString());
            int key2 = Integer.parseInt(itr2.next().toString());
            child1[key1] = Median(Integer.parseInt(id1.get(key1).toString()), Integer.parseInt(id2.get(key2).toString()));
            child2[key1] = child1[key1];
        }
        }
        // Inserindo o child na offspring
        offspring[childCounter] = child1;
        childCounter++; // Atualiza o contador após inserir o child na offspring
         offspring[childCounter] = child2;
        childCounter++; // Atualiza o contador após inserir o child na offspring
        
//        Testa a saida comparando o pai com o filho e os hashs
        System.out.println("id1 " + id1);
        System.out.println("id2 " + id2);
        System.out.println("Pai 1 " + Arrays.toString(father1));
        System.out.println("Fil 1 " + Arrays.toString(child1));
        System.out.println("Pai 2 " + Arrays.toString(father2));
        System.out.println("Fil 2 " + Arrays.toString(child2));
    }

    // Calcula a mediana entre dos inteiros ao criar uma lista entre os inteiros menor e maior
    public int Median(int major, int minus) {
        int median = 0;
        int mn = minus;
        int mj = major; // Memoria para guardar os valores maior e menor.
        if (major == minus) {
            median = major;
        } else {
            ArrayList AL = new ArrayList<>(); // Lista para armazenar os valores intervalares e os valores menor e o maior
            major = Math.max(mj, mn); // atribui o maior valor ao major
            minus = Math.min(mj, mn); // atribui o menor valor ao minus
            for (int i = minus; i <= major; i++) { // Preenche a lista com os valores maior, menor e intervalares
                AL.add(i);
            }
            if (AL.size() % 2 == 1) { // ver se é impar o tamanho da lista
                int middle = (AL.size() - 1) / 2;
                median = (int) AL.get(middle);
            } else { // Se for par o valor da mediana é atribuido segundo a probabilidade de 50% para o meio - 1 e o meio.
                int middle = (AL.size()) / 2;
//                if (r.nextDouble() <= 0.5) {
                if (Main.r.nextDouble() <= 0.5) {
                    median = (int) AL.get(middle);
                } else {
                    median = (int) AL.get(middle - 1);
                }
            }
        }

        return median;
    }

    public static void main(String[] args) {
        CrossOver co = new CrossOver();
        System.out.println(co.Median(1, 1) + " " + co.Median(1, 2) + " " + co.Median(1, 3));
    }

}

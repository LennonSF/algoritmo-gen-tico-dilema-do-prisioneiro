/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author LennonSF
 */
public class GeneticAlgorithm {  // todos os metodos do ag aqui
    
//  Var do torneiro  
    public ArrayList MatingPool = new ArrayList<>();
    
//  Vars do CrossOver
    private HashMap<Integer, Integer> Individual1;
    private HashMap<Integer, Integer> Individual2;
    private int[][] offspring;
    private int[] child1 , child2;
    public static int childCounter;
    
    
    
    public GeneticAlgorithm(){
        Individual1 = new HashMap<Integer, Integer>();
        Individual2 = new HashMap<Integer, Integer>();
        offspring = new int[Config.ChildPopSize][Config.CromossomeSize];
        child1 = new int[Config.CromossomeSize];
        child2 = new int[Config.CromossomeSize];
        childCounter = 0;
    
    }


// Torneio inicio    
public void Tournament(){
//        ArrayList ASI = new ArrayList<>(); // ASI - AlreadySelectedIndividuals
        ArrayList RSI = new ArrayList<>(); // RSI - RingueSelectedIndividuals
        ArrayList RSII = new ArrayList<>(); // RSI - RingueSelectedIndividualsIdenx
        int index = 0; //Salva o index no matingPool
        int temp = 0; 
        while (MatingPool.size() < Config.MatingPoolSize) {
            RSI.clear();
            RSII.clear();
            for (int i = 0; i < Config.RingueSize; i++) {
                temp = Main.r.nextInt(Config.PopulationSize);
                if (!RSI.contains(temp)) {
                RSI.add(Main.p.FitnessIndividual[temp]);
                RSII.add(temp);
                }
               }
            index = (int) RSII.get(RSI.indexOf(Collections.max(RSI)));
            if (!MatingPool.contains(index)) {
                MatingPool.add(index);
            }
        }
//        MatingPool = ASI;
        System.out.println(" Torneio " + MatingPool);
    }
// Torneio Fim    

// Metodos do CrossOver Inicio
// Seleciona os Genes de dois individuos que vão participar do cruzamento
//    public void SelectGenes(int[] ind1, int[] ind2) {
    public void SelectGenes() {
        int[] ind1;
        int[] ind2;
        int cind1 = 0; //contador dos genes selecionados do individuo.
        childCounter = 0;
        

        for (int k = 0; k < Config.MatingPoolSize; k = k + 2) {
            ind1 = Main.p.GetPopulation()[(int)MatingPool.get(k)];
            ind2 = Main.p.GetPopulation()[(int)MatingPool.get(k+1)];
            Individual1.clear();
            Individual2.clear(); // Limpa os hashs
//        while (cind1 < Config.ChildPopSize) {
            for (int i = 0; i < ind1.length; i++) {
                if ((Main.r.nextDouble() <= 0.5) && (!Individual1.containsKey(i))) { // probabilidade do gene ser selecionado
                    cind1++;
                    Individual1.put(i, ind1[i]);
                    Individual2.put(i, ind2[i]);
                }
//                if (cind1 == Config.ChildPopSize) {
//                    break;
//                }
            }
//        }

        MedianCrossOver(ind1,ind2,Individual1, Individual2);
    }
    }

// Tira a mediana do cruzamento e preeenche o filho e as proles
    public void MedianCrossOver(int[] father1 , int[] father2, HashMap id1, HashMap id2) {
        child1 = father1.clone();  // Copia todos os genes do pai para o filho e depois apenas altera os genes modificados pelo cruzamento por mediana
        child2 = father2.clone();  // Copia todos os genes do pai para o filho e depois apenas altera os genes modificados pelo cruzamento por mediana
        //A função clone passa os valores do vetor pai para o filho. E não a referência pois a referência seria alterada caso fosse passada quando o filho fosse alterado. Assim pai e filho seriam identicos 
        Iterator itr1 = id1.keySet().iterator();
        Iterator itr2 = id2.keySet().iterator();
        
        if (Main.r.nextDouble() <= 0.8) {  
        while (itr1.hasNext()) {
            int key1 = Integer.parseInt(itr1.next().toString());
            int key2 = Integer.parseInt(itr2.next().toString());
            child1[key1] = Median(Integer.parseInt(id1.get(key1).toString()), Integer.parseInt(id2.get(key2).toString()));
            child2[key1] = child1[key1];
        }
        }
        // Inserindo o child na offspring
        offspring[childCounter] = child1;
        childCounter++; // Atualiza o contador após inserir o child na offspring
        offspring[childCounter] = child2;
        childCounter++; // Atualiza o contador após inserir o child na offspring
        
//        Testa a saida comparando o pai com o filho e os hashs
        System.out.println("id1 " + id1);
        System.out.println("id2 " + id2);
        System.out.println("Pai 1 " + Arrays.toString(father1));
        System.out.println("Fil 1 " + Arrays.toString(child1));
        System.out.println("Pai 2 " + Arrays.toString(father2));
        System.out.println("Fil 2 " + Arrays.toString(child2));
    }

    // Calcula a mediana entre dos inteiros ao criar uma lista entre os inteiros menor e maior
    public int Median(int major, int minus) {
        int median = 0;
        int mn = minus;
        int mj = major; // Memoria para guardar os valores maior e menor.
        if (major == minus) {
            median = major;
        } else {
            ArrayList AL = new ArrayList<>(); // Lista para armazenar os valores intervalares e os valores menor e o maior
            major = Math.max(mj, mn); // atribui o maior valor ao major
            minus = Math.min(mj, mn); // atribui o menor valor ao minus
            for (int i = minus; i <= major; i++) { // Preenche a lista com os valores maior, menor e intervalares
                AL.add(i);
            }
            if (AL.size() % 2 == 1) { // ver se é impar o tamanho da lista
                int middle = (AL.size() - 1) / 2;
                median = (int) AL.get(middle);
            } else { // Se for par o valor da mediana é atribuido segundo a probabilidade de 50% para o meio - 1 e o meio.
                int middle = (AL.size()) / 2;
//                if (r.nextDouble() <= 0.5) {
                if (Main.r.nextDouble() <= 0.5) {
                    median = (int) AL.get(middle);
                } else {
                    median = (int) AL.get(middle - 1);
                }
            }
        }

        return median;
    }
    
    // Metodos do CrossOver Fim
    
    
// Metodos da Mutação
//    public void Mutation(int[][] matrix2d){
    public void Mutation(){

    for (int i = 0; i < offspring.length; i++) { // antes do childpopsize tava 40
        for (int j = 0; j < Config.CromossomeSize; j++) { //Tava com 30 ver se funciona desse jeito
            if (Main.r.nextDouble() <= Config.mtProbability) {
               
//               int numero = Creep(matrix2d[i][j]);
               int numero = Creep(offspring[i][j]);
//               System.out.println("Antes do mt " + matrix2d[i][j] + " Depois do mt " + numero);
               System.out.println("Antes do mt " + offspring[i][j] + " Depois do mt " + numero);
//               matrix2d[i][j] = numero;
               offspring[i][j] = numero;
            }
        }
    }
    
}

private int Creep(int n){
int np1, np2, np3; // np = numberPlus
int nm1, nm2, nm3; // nm = numberMinus

    //Determina a vizinhança inicio
    if (n == 9)
        np1 = 0;
    else
        np1 = n+1;
    
    if (np1 == 9)
        np2 = 0;
    else
        np2 = np1+1;
    
    if (np2 == 9)
        np3 = 0;
    else
        np3 = np2+1;
    
    if (n == 0) 
        nm1 = 9;
    else
        nm1 = n-1;
    
    if (nm1 == 0)
        nm2 = 9;
    else
        nm2 = nm1-1;
    
    if (nm2 == 0)
        nm3 = 9;
    else
        nm3 = nm2-1;
    //Determina a vizinhança fim
    
    //Aplica Mutação inicio
    
    double probability = Main.r.nextDouble();
    
    if (probability <= 0.6) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np1;
        }else
            n = nm1;
    }else if (probability > 0.6 && probability <= 0.9) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np2;
        }else
            n = nm2;
    }else if (probability > 0.9) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np3;
        }else
            n = nm3;
    }
    
    //Aplica Mutação fim
    
return n;
}
// Métodos da muatação Fim

//Seleção de sobreviventes e atualização da população
public void Survivors(){
    int counter = 0;
    for (int i = 0; i < Config.MatingPoolSize; i++) {
        if(!MatingPool.contains(i)){
            Main.p.GetPopulation()[i] = offspring[counter].clone();
            counter++;
        } 
    }
  }

}    
 

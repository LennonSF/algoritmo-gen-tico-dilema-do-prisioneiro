/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

/**
 *
 * @author lennonsalesfurtado
 */
public class Main {
    public static Population p = new Population();
    public static MersenneTwisterFast r;
    public static GeneticAlgorithm ag = new GeneticAlgorithm();
    
    public static void main(String[] args) {
       r = new MersenneTwisterFast();
//       Mutation mt = new Mutation();
//       CrossOver cr = new CrossOver();
      
       
//       Cria a pop
        p.CreatePopulation();
        int[] group = new int[10];
        for (int i = 0; i < 10; i++) {
            
        
//       Calcula o Fitness
       Population.FitnessPair(p.GetPopulation());
//       Population.Fitness10Porcent(p.GetPopulation());
//       Population.Fitness80Porcent(p.GetPopulation());

//     Verificar a codição de parada

//      Aplica o torneio para gerar o matingpool
       ag.Tournament();
              System.out.println("F 1" + Population.FitnessIndividual[0]);
//      Apica o CrossOver tendo como saida vetor offsprings com as proles dos pais 
        //       cr.SelectGenes(a,b);
        //       int[] a = p.GetPopulation()[0];
        //       int[] b = p.GetPopulation()[1];
        ag.SelectGenes();
               System.out.println("F 2" + Population.FitnessIndividual[0]);
       
//      Aplica a Mutação nas proles
        //       mt.Mutation(p.GetPopulation());
        ag.Mutation();
               System.out.println("F 3" + Population.FitnessIndividual[0]);
//        Substitui a população pelos sobreviventes
        ag.Survivors();
//       Plota os gráficos
//       View.PrintPopulation(p);
           System.out.println("F 4" + Population.FitnessIndividual[0]);
//        group[i] = Population.FitnessIndividual[0];
        group[i] = Population.FitnessGroup;
        }
         System.out.println("F U " + group[1]);
        View.PloterScatterPlot(group);
//        View.PloterLinePlot(group);
        
        
        
    }
    
}
//Duvidas
//O que deve ser imprimido o fitness individual ou do grupo

//Melhorias
// Trocar o scatterplot por lineplot
// Colocar o lineplot na view

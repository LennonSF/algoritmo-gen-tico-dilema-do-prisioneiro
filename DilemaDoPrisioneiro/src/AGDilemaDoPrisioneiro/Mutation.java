/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

/**
 *
 * @author lennonsalesfurtado
 */
public class Mutation {
    
   
   
public Mutation(){

}   

public void Mutation(int[][] matrix2d){

    for (int i = 0; i < 40; i++) {
        for (int j = 0; j < matrix2d[j].length; j++) { //Tava com 30 ver se funciona desse jeito
            if (Main.r.nextDouble()<= Config.mtProbability) {
               
               int numero = Creep(matrix2d[i][j]);
               System.out.println("Antes do mt " + matrix2d[i][j] + " Depois do mt " + numero);
               matrix2d[i][j] = numero;
            }
        }
    }
    
}

private int Creep(int n){
int np1, np2, np3; // np = numberPlus
int nm1, nm2, nm3; // nm = numberMinus

    //Determina a vizinhança inicio
    if (n == 9)
        np1 = 0;
    else
        np1 = n+1;
    
    if (np1 == 9)
        np2 = 0;
    else
        np2 = np1+1;
    
    if (np2 == 9)
        np3 = 0;
    else
        np3 = np2+1;
    
    if (n == 0) 
        nm1 = 9;
    else
        nm1 = n-1;
    
    if (nm1 == 0)
        nm2 = 9;
    else
        nm2 = nm1-1;
    
    if (nm2 == 0)
        nm3 = 9;
    else
        nm3 = nm2-1;
    //Determina a vizinhança fim
    
    //Aplica Mutação inicio
    
    double probability = Main.r.nextDouble();
    
    if (probability <= 0.6) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np1;
        }else
            n = nm1;
    }else if (probability > 0.6 && probability <= 0.9) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np2;
        }else
            n = nm2;
    }else if (probability > 0.9) {
        if (Main.r.nextDouble() <= 0.5) {
            n = np3;
        }else
            n = nm3;
    }
    
    //Aplica Mutação fim
    
return n;
}

}

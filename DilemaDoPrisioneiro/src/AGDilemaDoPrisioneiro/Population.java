package AGDilemaDoPrisioneiro;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author lennonsalesfurtado
 */
public class Population {

    public static int[] FitnessIndividual;
    public static int FitnessGroup;
    public static int FitnessGroupWithBonus;
    private int[][] population;
    public static int PopulationSize;
    public static int PS10; // PS - Population Size 
    public static int PS80;
    public static int CromossomeSize;
    public static int BonusCount;
    public static int BonusPts;
    
    public Population() {
        PopulationSize = 40;
        CromossomeSize = 30;
        PS10 = 4;
        PS80 = 32;
        population = new int[PopulationSize][CromossomeSize];
        FitnessGroup = 0;
        FitnessGroupWithBonus = 0;
        FitnessIndividual = new int[PopulationSize];
        BonusCount = 0;
        BonusPts = 10;
        
    }

    public void CreatePopulation() {
        for (int i = 0; i < PopulationSize; i++) {
            CreateIndividual(i);
        }
    }

    private void CreateIndividual(int PopIndex) {
//    MersenneTwisterFast r;
//    r = new MersenneTwisterFast(); // para testes locais usar este
        for (int i = 0; i < CromossomeSize; i++) {
//        population[PopIndex][i] = r.nextInt(10);
            population[PopIndex][i] = Main.r.nextInt(10);

        }
    }

    public int[][] GetPopulation() {
        return population;
    }

    public static void UpdatePopulation() {
        // Ao susbstituir o a população pelas proles, resetar o offspring e o childcount da classe CrossOver

    }

    public static void FitnessPair(int[][] pop) {
        ArrayList ASP = new ArrayList<>(); // ASP - AlreadySelectedPairs
        int index1 = 0;
        int index2 = 0;
        FitnessGroup = 0;
        Arrays.fill(FitnessIndividual, 0);
        
        while (ASP.size() < PopulationSize) {
            index1 = Main.r.nextInt(40);
            index2 = Main.r.nextInt(40);
            int fitInd1 = 0;
            int fitInd2 = 0;
            if (index1 != index2) {
                if (!ASP.contains(index1) && (!ASP.contains(index2))) {
                    for (int i = 0; i < pop[index1].length; i++) { // testar este index length pra ver se é 40
                        fitInd1 += DC(pop[index1][i], pop[index2][i])[0];
                        fitInd2 += DC(pop[index1][i], pop[index2][i])[1];
                    }
                    FitnessIndividual[index1] = fitInd1;
                    FitnessIndividual[index2] = fitInd2;
                    Bonus(pop[index1]);
                    Bonus(pop[index2]);
                    ASP.add(index1);
                    ASP.add(index2);
                }
            }
        }
        FitnessGroupWithBonus += FitnessGroup;
        System.out.println(" Individuo " + Arrays.toString(FitnessIndividual) + " Grupo sem bônus " + FitnessGroup + " Grupo com Bônus " + FitnessGroupWithBonus);
    
    }

    public static void Fitness10Porcent(int[][] pop) {
        ArrayList ASI = new ArrayList<>(); // ASI - AlreadySelectedIndividuals
        ArrayList ASP = new ArrayList<>(); // ASP - AlreadySelectedPairs
        int index1 = 0;
        int index2 = 0;
        FitnessGroup = 0;
        Arrays.fill(FitnessIndividual, 0);
        
        while (ASI.size() < PopulationSize) {
            index1 = Main.r.nextInt(40);
            int fitInd1 = 0;
            if (!ASI.contains(index1)) {
                while (ASP.size() < PS10) {
                    index2 = Main.r.nextInt(40);

                    if (index1 != index2) {
                        if (!ASP.contains(index2)) {
                            for (int i = 0; i < pop[index1].length; i++) {
                                fitInd1 += DC(pop[index1][i], pop[index2][i])[0];
                            }

                            ASP.add(index2);
                        }
                    }
                }
               
                ASP.clear();
                FitnessIndividual[index1] = fitInd1/PS10;
                Bonus(pop[index1]);
                ASI.add(index1);
            }
        }
         FitnessGroup = FitnessGroup/PS10;
         FitnessGroupWithBonus += FitnessGroup;
        System.out.println(" Individuo " + Arrays.toString(FitnessIndividual) + " Grupo sem bônus " + FitnessGroup + " Grupo com Bônus " + FitnessGroupWithBonus);
    
    }
    
    public static void Fitness80Porcent(int[][] pop) {
        ArrayList ASI = new ArrayList<>(); // ASI - AlreadySelectedIndividuals
        ArrayList ASP = new ArrayList<>(); // ASP - AlreadySelectedPairs
        int index1 = 0;
        int index2 = 0;
        FitnessGroup = 0;
        Arrays.fill(FitnessIndividual, 0);
        
        while (ASI.size() < PopulationSize) {
            index1 = Main.r.nextInt(40);
            int fitInd1 = 0;
            if (!ASI.contains(index1)) {
                while (ASP.size() < PS80) {
                    index2 = Main.r.nextInt(40);

                    if (index1 != index2) {
                        if (!ASP.contains(index2)) {
                            for (int i = 0; i < pop[index1].length; i++) {
                                fitInd1 += DC(pop[index1][i], pop[index2][i])[0];
                            }

                            ASP.add(index2);
                        }
                    }
                }
                ASP.clear();
                FitnessIndividual[index1] = fitInd1/PS80;
                Bonus(pop[index1]); // Ler o individuo para aplicar o bônus no grupo
                ASI.add(index1);
                
            }
        }
        FitnessGroup = FitnessGroup/PS80;
        FitnessGroupWithBonus += FitnessGroup;
        System.out.println(" Individuo " + Arrays.toString(FitnessIndividual) + " Grupo sem bônus " + FitnessGroup + " Grupo com Bônus " + FitnessGroupWithBonus);
    }

    public static int[] DC(int dc1, int dc2) { // DC - denounce and cooperate
        int[] fitness = new int[2];
        if ((dc1 <= 4) && (dc2 <= 4)) {
            FitnessGroup += 10;
            fitness[0] = 5;
            fitness[1] = 5;
           
        } else if ((dc1 <= 4) && (dc2 >= 5)) {
            FitnessGroup += 5;
            fitness[0] = 0;
            fitness[1] = 10;
        } else if ((dc1 >= 5) && (dc2 <= 4)) {
            FitnessGroup += 5;
            fitness[0] = 10;
            fitness[1] = 0;
        } else if ((dc1 >= 5) && (dc2 >= 5)) {
            FitnessGroup += 0;
            fitness[0] = 2;
            fitness[1] = 2;
        }
        return fitness;
    }

    public static void Bonus(int[] ind) {
        BonusCount = 0;
        int count = 0;
        for (int i = 0; i < ind.length; i++) {
            if (ind[i] <= 4) {
                BonusCount++;
                count++;
                if (BonusCount == 3) { // Se tiver três cooperações seguidas o bônus é somado ao fitness do grupo
                    FitnessGroupWithBonus += BonusPts;
                    System.out.println(" Ganhou Bônus " + count);
                    BonusCount = 0;
                }
            }
        }
    }
}
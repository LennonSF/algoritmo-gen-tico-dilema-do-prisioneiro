/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AGDilemaDoPrisioneiro;

import java.awt.Color;
import java.util.Arrays;
import javax.swing.JFrame;
import org.math.plot.FrameView;
import org.math.plot.Plot2DPanel;
import org.math.plot.PlotPanel;
import org.math.plot.plots.LinePlot;
import org.math.plot.plots.ScatterPlot;

/**
 *
 * @author lennonsalesfurtado
 */
public class View {
    
    public static void PrintPopulation(Population p){
//        System.out.println(Arrays.toString(p.GetPopulation())); // não está funcionando
int[][] pop = p.GetPopulation();
        for (int i = 0; i < 40; i++) {
            System.out.println("Individuo" + i);
            for (int j = 0; j < 30; j++) {
                System.out.println(pop[i][j]);
            }
        }
    }
    
    public static void PloterScatterPlot(int[] a){
             Plot2DPanel p = new Plot2DPanel();
                         double[][] XYZ = new double[a.length][2];
                         for (int j = 0; j < XYZ.length; j++) {
                                 XYZ[j][0] = /*1 + */a[j];
                                 XYZ[j][1] = /*100 * */a[j];
                         }
                         ScatterPlot p2 = new ScatterPlot("Fitness", Color.BLUE, XYZ);
                         
                         p.addPlot(p2);

                 p.setLegendOrientation(PlotPanel.SOUTH);
                 new FrameView(p).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         }
    
    public static void PloterLinePlot(int[] a){
             Plot2DPanel p = new Plot2DPanel();
                         double[][] XYZ = new double[a.length][2];
                         for (int j = 0; j < XYZ.length; j++) {
                                 XYZ[j][0] = /*1 + */a[j];
                                 XYZ[j][1] = /*100 * */a[j];
                         }
                         LinePlot p2 = new LinePlot("Fitness", Color.BLUE, XYZ);
                         
                         p.addPlot(p2);

                 p.setLegendOrientation(PlotPanel.SOUTH);
                 new FrameView(p).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         }
    
}
